//
//  ViewController.swift
//  Calculator App
//
//  Created by Muhamad Widi Aryanto on 15/08/19.
//  Copyright © 2019 Widi Aryanto. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var textLabel: UILabel!
    
    var numberOnScreen: Double = 0
    var previousNumber: Double = 0
    var operation = 0
    var performingMath = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func numbers(_ sender: UIButton) {
        if performingMath == true {
            textLabel.text = String(sender.tag - 1)
            numberOnScreen = Double(textLabel.text!)!
            performingMath = false
        } else {
            textLabel.text = textLabel.text! + String(sender.tag - 1)
            numberOnScreen = Double(textLabel.text!)!
        }
    }
    
    @IBAction func buttons(_ sender: UIButton) {
        if textLabel.text != "" && sender.tag != 15 && sender.tag != 16 {
            previousNumber = Double(textLabel.text!)!
            // Pembagian
            if sender.tag == 11 {
                textLabel.text = "/"
            // Perkalian
            } else if sender.tag == 12 {
                textLabel.text = "X"
            // Pengurangan
            } else if sender.tag == 13 {
                textLabel.text = "-"
            // Penjumlahan
            } else if sender.tag == 14 {
                textLabel.text = "+"
            }
            operation = sender.tag
            performingMath = true
        } else if sender.tag == 15 {
            // Pembagian
            if operation == 11 {
                textLabel.text = String(previousNumber / numberOnScreen)
            // Perkalian
            } else if operation == 12 {
                textLabel.text = String(previousNumber * numberOnScreen)
            // Pengurangan
            } else if operation == 13 {
                textLabel.text = String(previousNumber - numberOnScreen)
            // Penjumlahan
            } else if operation == 14 {
                textLabel.text = String(previousNumber + numberOnScreen)
            }
        } else if sender.tag == 16 {
            textLabel.text = ""
            previousNumber = 0
            numberOnScreen = 0
            operation = 0
        }
    }
}

